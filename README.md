# Nagłówek

Paragraf 1.

Paragraf 2.

Paragraf 3.

To jest ~~przekreślenie~~, **pogrubienie**, *kursywa*.
> To jest cytat

1. pozycja 1
2. pozycja 2
    1. Podlista 1
    2. Podlista 2
3. pozycja 3

+ pozycja 1
+ pozycja 2
    + podpozycja 1
    + podpozycja 2
+ pozycja 3

```
To jest blok kodu
cześć dalsza
i koniec kodu.
```

A tu jest trochę teskstu w którym jest kod, `print("Kod w tekście") ` lorem ipsum dolos mater.


![img/pingwin.png](img/pingwin.png)
