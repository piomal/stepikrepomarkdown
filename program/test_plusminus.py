from dodawanie import dodawanie
from odejmowanie import odejmowanie


def test_dodawanie():
    assert dodawanie(3, 3) == 6

def test_dodawanie2():
    assert dodawanie(10, -1) == 9

def test_odejmowanie():
    assert odejmowanie(8, 5) == 3

def test_odejmowanie2():
    assert odejmowanie(100, 10) == 90